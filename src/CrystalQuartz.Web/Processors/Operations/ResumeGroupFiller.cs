namespace CrystalQuartz.Web.Processors.Operations
{
    using System.Web;
    using Core;
    using Core.SchedulerProviders;

    public class ResumeGroupFiller : OperationFiller
    {
        public ResumeGroupFiller(ISchedulerProvider schedulerProvider)
            : base(schedulerProvider)
        {
        }

        protected override void DoAction(HttpResponseBase response, HttpContextBase context)
        {
            var jobGroup = context.Request.Params["group"];
            var groupMatcher = Quartz.Impl.Matchers.GroupMatcher<Quartz.JobKey>.GroupContains(jobGroup);

            _schedulerProvider.Scheduler.ResumeJobs(groupMatcher);
        }
    }
}